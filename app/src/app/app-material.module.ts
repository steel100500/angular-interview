
import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatSelectModule,
  MatInputModule, MatToolbarModule,
  MatTableModule, MatDividerModule } from "@angular/material";
import { MatSortModule } from '@angular/material/sort';


@NgModule({
  declarations: [],
  imports: [
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatSelectModule,
    MatTableModule,
    MatSortModule,
    MatDividerModule
  ],
  exports: [
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatSelectModule,
    MatTableModule,
    MatSortModule,
    MatDividerModule
  ],
})
export class AppMaterialModule { }
