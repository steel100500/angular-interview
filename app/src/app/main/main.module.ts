import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { AppMaterialModule } from '../app-material.module';



@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule
  ],
  declarations: [MainComponent],
  entryComponents: [MainComponent]
})
export class MainModule { }
