import { Component, OnInit } from '@angular/core';

export class Phone {
  constructor(public title: string,
              public price: number,
              public company: string) { }
}

@Component({
  selector: 'app-div',
  templateUrl: './div.component.html',
  styleUrls: ['./div.component.scss']
})
export class DivComponent {
  title:string = '';
  phone: Phone = new Phone("", 0, "Huawei");
  phones: Phone[] = [];
  companies: string[] = ["Apple", "Huawei", "Xiaomi", "Samsung", "LG", "Motorola", "Alcatel"];

  addPhone() {
      this.phones.push(new Phone(this.phone.title, this.phone.price, this.phone.company));
  }
}
