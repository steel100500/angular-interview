import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DivComponent } from './div.component';
import { AppMaterialModule } from '../app-material.module';
import { NavbarModule } from '../navbar/navbar.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [DivComponent],
  imports: [
    CommonModule,
    AppMaterialModule,
    NavbarModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class DivModule { }
