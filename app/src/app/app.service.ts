import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  public router: Router;

  constructor( router: Router) {
    this.router = router;
  }
}
