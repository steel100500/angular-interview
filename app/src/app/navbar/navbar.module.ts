import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar.component';
import { AppMaterialModule } from '../app-material.module';



@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule
  ],
  exports: [NavbarComponent],
  declarations: [NavbarComponent],
  entryComponents: [NavbarComponent],
})
export class NavbarModule { }
