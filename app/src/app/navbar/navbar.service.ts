import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {
  public router: Router;

  constructor( router: Router) {
    this.router = router;
   }
}
